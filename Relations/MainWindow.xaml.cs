﻿using Esri.ArcGISRuntime.Data;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Relations
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RelationHelper helper;

        string PATH;
        public MainWindow()
        {
            InitializeComponent();
            this.Dispatcher.UnhandledException += Dispatcher_UnhandledException;
        }

        private void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(string.Format("Exception Thrown: {0}", e.Exception.Message));
        }

        public async void Init()
        {
            helper = new RelationHelper(PATH);
            await helper.Init();
            tablesLB.SelectedItem = null;
            idsLB.SelectedItem = null;
            tablesLB.ItemsSource = helper.Tables;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                long id = long.Parse(idTB.Text);
                FeatureTable table = tablesLB.SelectedItem as FeatureTable;
                string result = await helper.Query(id, table);
                resultText.Text = result;
            }
            catch (Exception)
            {
                
            }
        }

        private void tablesLB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                FeatureTable selectedTable = e.AddedItems[0] as FeatureTable;
                ShowIds(selectedTable);
            }
        }

        private async void ShowIds(FeatureTable table)
        {
            var features = await table.QueryAsync(new QueryFilter()
            {
                WhereClause = "1=1"
            });

            idsLB.ItemsSource = features.Select(f => f.Attributes[table.ObjectIDField]).Cast<long>();
        }

        private void idsLB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                int id = int.Parse(e.AddedItems[0].ToString());
                idTB.Text = id.ToString();
            }
        }

        private void PickBtn_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();

            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            dialog.DefaultExt = ".geodatabase";
            dialog.Filter = "Geodatabase (*.geodatabase)|*.geodatabase";

            Nullable<bool> result = dialog.ShowDialog();

            if (result == true)
            {
                string filename = dialog.FileName;
                PATH = filename;
                Init();
            }
        }
    }
}
