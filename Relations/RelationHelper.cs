﻿using Esri.ArcGISRuntime.ArcGISServices;
using Esri.ArcGISRuntime.Data;
using Esri.ArcGISRuntime.Tasks.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Relations
{
    class RelationHelper
    {
        string PATH;   
        Geodatabase gdb;
        public IEnumerable<FeatureTable> Tables;

        public RelationHelper(string dbPath)
        {
            PATH = dbPath;
        }

        public async Task<bool> Init()
        {
            return await InitData();
        }
       
        private async Task<bool> InitData()
        {
            gdb = await Geodatabase.OpenAsync(PATH);
            Tables = GetFeatureTables();
            return await Task.FromResult<bool>(true);
        }

        private IEnumerable<FeatureTable> GetFeatureTables()
        {
            foreach (FeatureTable table in gdb.FeatureTables)
            {
                yield return table;
            }
        }

        public async Task<string> Query(long id, FeatureTable table)
        {
            ArcGISFeatureTable tab = table as ArcGISFeatureTable;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Table: {0}; Feature OBJECTID: {1}", table.Name, id.ToString()));
            foreach (Relationship relation in tab.ServiceInfo.Relationships)
            {
                try
                {
                    sb.AppendLine("=======================================");
                    GeodatabaseFeatureTable relatedTable = gdb.GetFeatureTableById((int)relation.RelatedTableID);
                    sb.AppendLine(string.Format("Name: {0}; Cardinality: {1}; Role: {2}; ID: {3}; RelatedTableID: {4}", 
                        relation.Name, relation.Cardinality.ToString(), relation.Role.ToString(), relation.ID, relation.RelatedTableID));

                    sb.AppendLine(string.Format("Related Table Name: {0}", relatedTable.Name));

                    IReadOnlyList<GeodatabaseFeature> features = await tab.QueryRelatedAsync(id, relation.ID);

                    sb.AppendLine(string.Format("Features Count: {0}", features.Count));
                }
                catch (Exception ex)
                {
                    sb.AppendLine(string.Format("Error: {0}", ex.Message));
                }
            }

            return sb.ToString();
        }
    }
}
